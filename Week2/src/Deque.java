import edu.princeton.cs.algs4.StdOut;
import java.util.Iterator;
import java.lang.Iterable;
import java.util.NoSuchElementException;

public class Deque<Item> implements Iterable<Item> {
    private int size=0;
    private Node head,tail;

    private class Node {
        private Item item;
        private Node next;
        private Node prev;
    }

    // construct an empty deque
    public Deque() {
        head = null;
        tail = null;
        size = 0;
    }

    // is the deque empty?
    public boolean isEmpty() {
       return (size == 0);
    }

    // return the number of items on the deque
    public int size() {
        return size;
    }

    // add the item to the front
    public void addFirst(Item item) {
        if (item == null) throw new java.lang.IllegalArgumentException();
        Node newNode = new Node();
        newNode.item = item;
        if (head == null) {
            head = newNode;
            tail = newNode;
        }
        else {
            newNode.next = head;
            head.prev = newNode;
            head = newNode;
        }
        size++;
    }

    // add the item to the back
    public void addLast(Item item) {
        if (item == null) throw new java.lang.IllegalArgumentException();
        Node newNode = new Node();
        newNode.item = item;
        if (head == null) {
            head = newNode;
            tail = newNode;
        }
        else {
            newNode.prev = tail;
            tail.next = newNode;
            tail = newNode;
        }
        size++;
    }

    // remove and return the item from the front
    public Item removeFirst() {
        if (head == null) throw new NoSuchElementException();
        Item item = head.item;
        head = head.next;
        if (head == null) tail = null;
        else head.prev = null;
        size--;
        return item;
    }

    // remove and return the item from the back
    public Item removeLast() {
        if (tail == null) throw new NoSuchElementException();
        Item item = tail.item;
        tail = tail.prev;
        if (tail == null) head = null;
        else tail.next = null;
        size--;
        return item;
    }

    // return an iterator over items in order from front to back
    public Iterator<Item> iterator() {
        return new ListIterator();
    }

    private class ListIterator implements Iterator<Item> {
        private Node current = head;
        public boolean hasNext(){
                return(current!=null);
        }
        public void remove(){
                throw new UnsupportedOperationException();
        }
        public Item next(){
                if(!hasNext())throw new NoSuchElementException();
                Item item = current.item;
                current = current.next;
                return item;
        }
    }

    // unit testing (required)
    public static void main(String[]args){
        Deque<String> deque = new Deque<String>();
        StdOut.print("Is the queue empty? " + deque.isEmpty() + "\n");
        StdOut.print("Let's add some things to the front: \n");
        deque.addFirst("Apples");
        deque.addFirst("Books");
        deque.addFirst("Crystals");
        deque.addFirst("Bananaslugs");
        StdOut.print("Empty? "+ deque.isEmpty() +" Size " + deque.size() +" STACK = ( ");
		for (String temp : deque) {
		    StdOut.print(" ");
		    StdOut.print(temp);
		} 
        StdOut.print(")\n");
        StdOut.print("Let's add some things to the back: \n");
        deque.addLast("Eric");
        deque.addLast("Fires");
        deque.addLast("Giant");
        deque.addLast("Horses");
        StdOut.print("Empty? "+ deque.isEmpty() +" Size " + deque.size() +" STACK = ( ");
		for (String temp : deque) {
		    StdOut.print(" ");
		    StdOut.print(temp);
		} 
        StdOut.print(") \n");
        
        StdOut.print("Removing the first node from the stack which is " + deque.removeFirst() + " \n");
        StdOut.print("Empty? "+ deque.isEmpty() +" Size " + deque.size() +" STACK = ( ");
		for (String temp : deque) {
		    StdOut.print(" ");
		    StdOut.print(temp);
		} 
        StdOut.print(") \n");
        
        StdOut.print("Removing the first node from the stack which is " + deque.removeFirst() + " \n");
        StdOut.print("Empty? "+ deque.isEmpty() +" Size " + deque.size() +" STACK = ( ");
		for (String temp : deque) {
		    StdOut.print(" ");
		    StdOut.print(temp);
		} 
        StdOut.print(") \n");
        
        StdOut.print("Removing the last node from the stack which is " + deque.removeLast() + " \n");
        StdOut.print("Empty? "+ deque.isEmpty() +" Size " + deque.size() +" STACK = ( ");
		for (String ntemp : deque) {
		    StdOut.print(" ");
		    StdOut.print(ntemp);
		} 
        StdOut.print(") \n");
        
        StdOut.print("Removing the last node from the stack which is " + deque.removeLast() + " \n");
        StdOut.print("Empty? "+ deque.isEmpty() +" Size " + deque.size() +" STACK = ( ");
		for (String mtemp : deque) {
		    StdOut.print(" ");
		    StdOut.print(mtemp);
		} 
        StdOut.print(") \n");
        
        StdOut.print("Removing the last node from the stack which is " + deque.removeLast() + " \n");
        StdOut.print("Empty? "+ deque.isEmpty() +" Size " + deque.size() +" STACK = ( ");
		for (String mtemp : deque) {
		    StdOut.print(" ");
		    StdOut.print(mtemp);
		} 
        StdOut.print(") \n");
        
        StdOut.print("Removing the last node from the stack which is " + deque.removeLast() + " \n");
        StdOut.print("Empty? "+ deque.isEmpty() +" Size " + deque.size() +" STACK = ( ");
		for (String mtemp : deque) {
		    StdOut.print(" ");
		    StdOut.print(mtemp);
		} 
        StdOut.print(") \n");
        
        StdOut.print("Removing the last node from the stack which is " + deque.removeLast() + " \n");
        StdOut.print("Empty? "+ deque.isEmpty() +" Size " + deque.size() +" STACK = ( ");
		for (String mtemp : deque) {
		    StdOut.print(" ");
		    StdOut.print(mtemp);
		} 
        StdOut.print(") \n");
        
        StdOut.print("Removing the last node from the stack which is " + deque.removeLast() + " \n");
        StdOut.print("Empty? "+ deque.isEmpty() +" Size " + deque.size() +" STACK = ( ");
		for (String mtemp : deque) {
		    StdOut.print(" ");
		    StdOut.print(mtemp);
		} 
        StdOut.print(") \n");
        
    }
}

