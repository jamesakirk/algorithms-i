import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class RandomizedQueue<Item> implements Iterable<Item> {

    // initial capacity of underlying resizing array
    private static final int INIT_CAPACITY = 8;

    private Item[] a;         // array of items
    private int n;            // number of elements on stack	

    public RandomizedQueue() {
    	a = (Item[]) new Object[INIT_CAPACITY];
    	n = 0;
    }
    
    private void resize(int capacity) {
        assert capacity >= n;
        // textbook implementation
        Item[] copy = (Item[]) new Object[capacity];
        for (int i = 0; i < n; i++) {
            copy[i] = a[i];
        }
        a = copy;
    }
    
    // is the randomized queue empty?
    public boolean isEmpty() {return n==0;}

    // return the number of items on the randomized queue
    public int size() { return n; }

    // add the item
    public void enqueue(Item item) {
    	if(item == null) throw new IllegalArgumentException();
        if (n == a.length) resize(2*a.length);  
        a[n++] = item;
    }

    // remove and return a random item
    public Item dequeue() {
    	if (n == 0) throw new NoSuchElementException();
    	int r = StdRandom.uniform(n);
    	Item item = a[r];
    	a[r] = a[n-1];
    	a[n-1] = null;
    	n--;
        if (n > 0 && n == a.length/4) resize(a.length/2);
        return item;
    }

    // return a random item (but do not remove it)
    public Item sample() {
    	if (n == 0) throw new NoSuchElementException();
    	int r = StdRandom.uniform(n);
    	return a[r];
    }

    // return an independent iterator over items in random order
    public Iterator<Item> iterator() {return new RandomIterator();}
    
    private class RandomIterator implements Iterator<Item> {
        private int i;
        // textbook implementation
        Item[] copy = (Item[]) new Object[n];
        
        public RandomIterator() {
            i = n-1;
            for (int j = 0; j < n; j++) copy[j] = a[j];
            StdRandom.shuffle(copy);
        }

        public boolean hasNext()  {
            return i >= 0;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

        public Item next() {
            if (!hasNext()) throw new NoSuchElementException();
            return copy[i--];
        }
    }
    
    // unit testing (required)
    public static void main(String[] args)
    {
 	   	StdOut.print("Random");
		RandomizedQueue<String> RQ = new RandomizedQueue<String>();
    	
        StdOut.print("Is the queue empty? " + RQ.isEmpty() + "\n");
        StdOut.print("Let's add some things to the front: \n");
        RQ.enqueue("Apples");
        RQ.enqueue("Crystals");
        RQ.enqueue("Bananaslugs");
        StdOut.print("Empty? "+ RQ.isEmpty() +" Size " + RQ.size() +" STACK = ( ");
		for (String temp : RQ) {
		    StdOut.print(" ");
		    StdOut.print(temp);
		} 
        StdOut.print(")\n");
        StdOut.print("Let's add some more things \n");
        RQ.enqueue("Eric");
        RQ.enqueue("Giant");
        RQ.enqueue("Horses");
        RQ.enqueue("AAA");
        RQ.enqueue("BBB");
        RQ.enqueue("CCC");
        RQ.enqueue("DDD");
        RQ.enqueue("EEE");
        RQ.enqueue("FFF");
        StdOut.print("Empty? "+ RQ.isEmpty() +" Size " + RQ.size() +" STACK = ( ");
		for (String temp : RQ) {
		    StdOut.print(" ");
		    StdOut.print(temp);
		} 
        StdOut.print(")\n");
        for(int i = 0; i < 40; i++) StdOut.print(RQ.sample() + "\n");
    }

}