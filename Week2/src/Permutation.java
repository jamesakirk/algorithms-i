import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

public class Permutation {
   public static void main(String[] args) {
		RandomizedQueue<String> RQ = new RandomizedQueue<String>();
        while (!StdIn.isEmpty()) {
            String item = StdIn.readString();
            RQ.enqueue(item);
        }
        int i = 0;
		for (String temp : RQ) {
			if(i < Integer.parseInt(args[0])){
			    StdOut.print(temp);
			    StdOut.print("\n");		
			    i++;
			}
		} 
   }
}