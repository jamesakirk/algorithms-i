import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;

public class RandomWord {
	public static void main (String[] args) {
		int itemsCounted = 0;
		String currentItem;
		String champion = " ";
		while (!StdIn.isEmpty()){
			currentItem = StdIn.readString();
			itemsCounted++;
			if(StdRandom.bernoulli(1./itemsCounted)) {
				champion = currentItem;
			}
		}
		StdOut.println(champion);
	}

}
