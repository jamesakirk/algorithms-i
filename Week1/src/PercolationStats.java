import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

public class PercolationStats {
	private static final double CONFIDENCE_95 = 1.96;
	private final int trials;
	private final double[] thresholds;

	public PercolationStats(int n, int trials) {
		if (n <= 0 || trials <= 0)
			throw new IllegalArgumentException();
		this.trials = trials;
		Percolation percolator;
		int x, y;
		thresholds = new double[trials];
		for (int i = 0; i < trials; i++) {
			percolator = new Percolation(n);
			while (!percolator.percolates()) {
				x = StdRandom.uniform(1, n + 1);
				y = StdRandom.uniform(1, n + 1);
				percolator.open(x, y);
			}
			thresholds[i] = percolator.numberOfOpenSites() * 1.0 / (n * n);
		}
	}

	public double mean() {
		return StdStats.mean(thresholds);
	}

	public double stddev() {
		return StdStats.stddev(thresholds);
	}

	public double confidenceLo() {
		return (mean() - CONFIDENCE_95 * stddev() / Math.sqrt(trials));
	}

	public double confidenceHi() {
		return (mean() + CONFIDENCE_95 * stddev() / Math.sqrt(trials));
	}

	public static void main(String[] args) {
		PercolationStats PS = new PercolationStats(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
		StdOut.print("mean\t\t\t= " + PS.mean() + "\nstddev\t\t\t= " + PS.stddev() + "\n95% confidence interval\t= ["
				+ PS.confidenceLo() + "," + PS.confidenceHi() + "]");
	}
}