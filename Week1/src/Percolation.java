/* This got 100/100, but failed the bonus:
 * 
	Estimated student memory = 17.00 n^2 + 0.00 n + 312.00   (R^2 = 1.000)
	Test 2 (bonus): check that total memory <= 11 n^2 + 128 n + 1024 bytes
	   -  failed memory test for n = 64
	==> FAILED
 * 
 * TODO
 * According to autograder feedback, I believe there is a way prevent backwash without a second UF object.
 * 
 */

import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {
    private final short size; // n x n percolation matrix. size means n.
    private final WeightedQuickUnionUF uFOPercolates;
    private final WeightedQuickUnionUF uFOFill;
    private boolean[] isOpen;
    private int numberOfOpenSites;    
    
    public Percolation(int n) {
        if (n <= 0) throw new IllegalArgumentException();
        size = (short) n;
        numberOfOpenSites = 0;
        isOpen = new boolean[n*n + 2]; // this array needs to have n*n+2 elements
        uFOPercolates = new WeightedQuickUnionUF (n*n + 2);
        uFOFill = new WeightedQuickUnionUF (n*n + 2);
        // first element a[0] is top virtual node, so open
        // last element a[n*n+1] is bottom virtual node, so open
        // others should start as closed
        for (int t = 0; t <= size*size + 1; t++) isOpen[t] = false;
        isOpen[0] = true;
        isOpen[size*size + 1] = true;
    }    

    public void open(int row, int col) {
        checkInput(row, col);
        // row and col are one-indexed
        // if(row > size || col> size){ //error}
        // this is a verb. make open.
        if (isOpen[convertCoords(row, col)]) return;
        isOpen[convertCoords(row, col)] = true;
        numberOfOpenSites++;      
        // UP:
        if (row == 1) {
            uFOPercolates.union(0, 	convertCoords(row,   col));
            uFOFill.union(      0, 	convertCoords(row,   col));
        } else {
            if (isOpen[             convertCoords(row-1, col)]) {
                uFOPercolates.union(convertCoords(row-1, col), convertCoords(row, col));
                uFOFill.union(      convertCoords(row-1, col), convertCoords(row, col));
            }
        }
        // DOWN:
        if (row == size) {
            uFOPercolates.union(size*size + 1, convertCoords(row, col));
        } else {
            if (isOpen[             convertCoords(row+1, col)]) {
                uFOPercolates.union(convertCoords(row+1, col), convertCoords(row, col));
                uFOFill.union(      convertCoords(row+1, col), convertCoords(row, col));
            }
        }
        // LEFT:
        if (col == 1) {
            // wall
        } else {
            if (isOpen[             convertCoords(row, col - 1)]) {
                uFOPercolates.union(convertCoords(row, col - 1), convertCoords(row, col));
                uFOFill.union(      convertCoords(row, col - 1), convertCoords(row, col));
            }
        }
        // RIGHT:
        if (col == size) {
            // wall
        } else {
            if (isOpen[             convertCoords(row, col + 1)]) {
                uFOPercolates.union(convertCoords(row, col + 1), convertCoords(row, col));
                uFOFill.union(      convertCoords(row, col + 1), convertCoords(row, col));
            }
        }
    }
    
    public boolean isOpen(int row, int col) {
        checkInput(row, col);
        return isOpen[convertCoords(row, col) ];
    }
    public boolean isFull(int row, int col) {
        checkInput(row, col);
        return (uFOFill.find(0) == uFOFill.find(convertCoords(row, col)));
    }
    public int numberOfOpenSites() {
        return numberOfOpenSites;
    }
    public boolean percolates() {
        int headComp = uFOPercolates.find(0);
        int tailComp = uFOPercolates.find(size*size + 1);
        return (headComp == tailComp);
    }
    public static void main(String[] args) {
        // does nothing
    }
    private void checkInput(int row, int col) {
        if (row < 1 || row > size || col < 1 || col > size) throw new IllegalArgumentException();
    }
    private int convertCoords(int row, int col) {
        return ((row-1)*size + (col-1) + 1);
    }
}

