/*
  * input5.txt
    - failed on trial 1391 of 10000
    - sequence of points inserted: 
      A  0.7 0.2
      B  0.5 0.4
      C  0.2 0.3
      D  0.4 0.7
      E  0.9 0.6
    - query point                   = (0.7, 0.936)
    - student   nearest()           = (0.7, 0.2)
    - reference nearest()           = (0.4, 0.7)
    - student   distanceSquaredTo() = 0.541696
    - reference distanceSquaredTo() = 0.145696

  * input10.txt
    - failed on trial 1182 of 10000
    - sequence of points inserted: 
      A  0.372 0.497
      B  0.564 0.413
      C  0.226 0.577
      D  0.144 0.179
      E  0.083 0.51
      F  0.32 0.708
      G  0.417 0.362
      H  0.862 0.825
      I  0.785 0.725
      J  0.499 0.208
    - query point                   = (0.947, 0.413)
    - student   nearest()           = (0.564, 0.413)
    - reference nearest()           = (0.785, 0.725)
    - student   distanceSquaredTo() = 0.146689
    - reference distanceSquaredTo() = 0.123588

==> FAILED
    
    
 */
	// search(p)
	// return searchRecur(root, p);
	
	// searchRecur(n, p)
	// if (n == null) // not found
	// if n.vertical
		// if point is left of node
			// search for closest on left
			// if a point on the right could be closer than this, then search right
		// if point is right of node
			// search for closest on right
			// if a point on the left could be closer than this, then search left
	// if n.horizontal
		// if point is below node
			// search for closest below
			// if a point aboe could be closer than this, then search above
		// if point is above node
			// search for closest above
			// if a point below could be closer than this, the search left
			


import java.util.ArrayList;
// search(p)
// return searchRecur(root, p);
import java.util.Stack;

// searchRecur(n, p)
// if (n == null) // not found
// if n.vertical
	// if point is left of node
		// search for closest on left
		// if a point on the right could be closer than this, then search right
	// if point is right of node
		// search for closest on right
		// if a point on the left could be closer than this, then search left
// if n.horizontal
	// if point is below node
		// search for closest below
		// if a point aboe could be closer than this, then search above
	// if point is above node
		// search for closest above
		// if a point below could be closer than this, the search left
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.StdDraw;

public class KdTree {
	private KdNode root;
	private int size = 0;
	
	
	private class KdNode {
		private final boolean horizontal;
		private final Point2D p;
		public KdNode right;
		public KdNode left;
		KdNode(boolean horizontal, Point2D p){
			this.horizontal = horizontal;
			this.p = p;
		}
	}

	public KdTree() {
	}
	public	int size() {return size;}                      // number of points in the set 
	public void insert(Point2D p){
		if(p == null) throw new IllegalArgumentException();

		if(root == null) {
			root = new KdNode(false, p);
			size = 1;
		} 
		if(!contains(p)) insertRecur(root, p);

	}           // add the point to the set (if it is not already in the set)
	private void insertRecur(KdNode n, Point2D p) {
		//if(n.p.x() == p.x() && n.p.y() == p.y()) return;
		if (n.p.equals(p)) return;
		if(n.horizontal) {
			if(p.y() < n.p.y()) {
				if(n.left == null) {
					n.left = new KdNode(!n.horizontal, p);
					size++;
				}
				else insertRecur(n.left, p);
			} else {
				if(n.right == null) {
					n.right = new KdNode(!n.horizontal, p);
					size++;
				}
				else insertRecur(n.right, p);
			}
		} else {
			if (p.x() < n.p.x()) {
				if (n.left == null) {
					n.left = new KdNode(!n.horizontal, p);
					size++;
				}
				else insertRecur(n.left, p);
			} else {
				if (n.right == null) {
					n.right = new KdNode(!n.horizontal, p);
					size++;
				}
				else insertRecur(n.right, p);			
			}
		}
	}
	// insert (p): 
	// if p==null throw IllegalArgumentException
	// if root == null then root = new Node (p);
	// else insertRecur(root, p)
	
	// insertRecur(node n, point p)
	// if n.horizontal
		// if point is below node
			// if n.left == null then n.left = new node(p)
			// else insertRecur(node n.left, point p)
		// if point is above node
			// if n.right == null then n.right = new node(p)
			// insertRecur(node n.right, point p)
	// if n.vertical
		// if point is left of node
			// if n.left == null then n.left = new node(p)new Point2D( 0.975528 , 0.345492 )
			// else insertRecur(node n.left, point p)
		// if point is right of node
			// if n.right == null then n.right = new node(p)
			// insertRecur(node n.right, point p)
	
	public boolean contains(Point2D p){
		if (p == null) throw new IllegalArgumentException();
		return containsRecur(root, p);
	}          // does the set contain point p? 

	private boolean containsRecur(KdNode n, Point2D p) {
		if (p == null) throw new IllegalArgumentException();
		if (n == null) return false;
//		if ((n.p.x() == p.x()) && (n.p.y() == p.y())) {
		if (n.p.equals(p)) {
			return true;
		}
		else {
			if(n.horizontal) {
				if (p.y() < n.p.y())
					return containsRecur(n.left, p);
				else
					return containsRecur(n.right, p);			
			}
			else {
				if (p.x() < n.p.x())
					return containsRecur(n.left, p);
				else
					return containsRecur(n.right, p);	
			}
		}
	}
	
	
	public void draw() {
		if(root != null)
			drawRecur(root, new Point2D(0.0,0.0), new Point2D(1.0,1.0));
	}                   // draw all points to standard draw 
	
	private void drawRecur(KdNode n, Point2D min, Point2D max) {
		if (n == null) return;
		// You draw the current node inside bounds
		// then, you change the bounds on right (or above),
		// then print left child
		// then, you change the bounds on left (or below),
		// then print left child
		// max of left child will be min of right child
		if(n.horizontal) {
			drawRecur(n.left, min, new Point2D(max.x(),n.p.y()));
			drawRecur(n.right, new Point2D(min.x(),n.p.y()), max);
			StdDraw.setPenColor(StdDraw.BLUE);
			StdDraw.setPenRadius();
			StdDraw.line(min.x(), n.p.y(), max.x(), n.p.y());
			StdDraw.setPenColor(StdDraw.BLACK);
			StdDraw.setPenRadius(0.01);
			n.p.draw();
		} else {	
			drawRecur(n.left, min, new Point2D(n.p.x(),max.y()));
			drawRecur(n.right, new Point2D(n.p.x(),min.y()), max);	
			StdDraw.setPenColor(StdDraw.RED);
			StdDraw.setPenRadius();
			StdDraw.line(n.p.x(), min.y(), n.p.x(), max.y());
			StdDraw.setPenColor(StdDraw.BLACK);
			StdDraw.setPenRadius(0.01);
			n.p.draw();
		}
	}
	
	
	public Iterable<Point2D> range(RectHV rect){
		if (rect == null) throw new IllegalArgumentException();
		if (root == null) return null;
		Stack<KdNode> Nodes = new Stack<KdNode>();
		Nodes = rangeRecur(root, rect, Nodes);
		Stack<Point2D> points = new Stack<Point2D>();
		for(KdNode n : Nodes) {
			points.add(n.p);
		}
		return points;
	}
	private Stack<KdNode> rangeRecur(KdNode n, RectHV r, Stack<KdNode> nodes) {
		if(r.contains(n.p)) nodes.add(n);
		if (!n.horizontal) {
			if (r.xmin() <= n.p.x())
				if (n.left != null) nodes = rangeRecur(n.left, r, nodes);
			if (n.p.x() <= r.xmax())
				if (n.right != null) nodes = rangeRecur(n.right, r, nodes);
		} else {
			if (r.ymin() <= n.p.y())
				if (n.left != null) nodes = rangeRecur(n.left, r, nodes);
			if (n.p.y() <= r.ymax())
				if (n.right != null) nodes = rangeRecur(n.right, r, nodes);
		}
		return nodes;
	}
	

		// stack<Node> s = new stack<node>();
		// range (node n, rect r)
		// if (r.cotains(n.p)) s.add(n);
		// if (n.vertical)
			// if (r.xmin < node.x)
				// if node.left != null
				// range (node.left)
			// if (node.x < r.max) 
				// if node.right != null	kdt.draw();
				// range (node.right)

		// if (n.horizontal)
			// if (r.ymin < node.y)
				// if node.left != null
				// range (node.left)
			// if (node.y < r.maxy) 
				// if node.right != null
				// range (node.right)	kdt.draw();

		
		
//		ArrayList<Point2D> ts = new ArrayList<Point2D>();
//		ts.add(new Point2D(0,0));
//		return ts;
//	}
	public Point2D nearest(Point2D p) {
		// TODO Impliment
		if (p == null) throw new IllegalArgumentException();
		if (root == null) return null;
		return nearestRecur(root, p);
	}
	private Point2D nearestRecur(KdNode n, Point2D p) {
		if (n == null) return null;
		Point2D closest = n.p;
		Point2D queryPoint;
		double dist = n.p.distanceTo(p);
		double queryDist;
		if(n.horizontal) {
			if(p.y() <= n.p.y()) {
				if(n.left != null) {
					queryPoint = nearestRecur(n.left, p);
					queryDist = queryPoint.distanceTo(p);
					if(queryDist < dist) {
						closest = queryPoint;
						dist = queryDist;
					}
				}
				if(n.p.y() <= (p.y() + dist) && n.right != null) {
					// there could be a point up there which is closer.
					queryPoint = nearestRecur(n.right, p);
					queryDist = queryPoint.distanceTo(p);
					if(queryDist < dist) {
						closest = queryPoint;
						dist = queryDist;
					}
				}
			}
			if(p.y() > n.p.y() ) {
				if(n.right != null) {
					queryPoint = nearestRecur(n.right, p);
					queryDist = queryPoint.distanceTo(p);
					if(queryDist < dist) {
						closest = queryPoint;
						dist = queryDist;
					}
				}
				if(p.y() < (n.p.y() + dist) && n.left != null) {
					// there could be a point down there which is closer.
					queryPoint = nearestRecur(n.left, p);
					queryDist = queryPoint.distanceTo(p);
					if(queryDist < dist) {
						closest = queryPoint;
						dist = queryDist;
					}
				}
				
			}
		}
		else {
			// vertical
			if(p.x() <= n.p.x()) {
				if(n.left != null) {
					queryPoint = nearestRecur(n.left, p);
					queryDist = queryPoint.distanceTo(p);
					if(queryDist < dist) {
						closest = queryPoint;
						dist = queryDist;
					}
				}
				if(n.p.x() <= (p.x() + dist) && n.right != null) {
					// there could be a point to the right which is closer.
					if (n.right != null) {
						queryPoint = nearestRecur(n.right, p);
						queryDist = queryPoint.distanceTo(p);
						if(queryDist < dist) {
							closest = queryPoint;
							dist = queryDist;
						}
					}
				}
			}
			if(p.x() > n.p.x()) {
				if (n.right != null) {
					queryPoint = nearestRecur(n.right, p);
					queryDist = queryPoint.distanceTo(p);
					if(queryDist < dist) {
						closest = queryPoint;
						dist = queryDist;
					}
				}
				if(p.x() < (n.p.x() + dist) && n.left != null) {
					// there could be a point down there which is closer.
					queryPoint = nearestRecur(n.left, p);
					queryDist = queryPoint.distanceTo(p);
					if(queryDist < dist) {
						closest = queryPoint;
						dist = queryDist;
					}
				}
			}
		} // vertical
		return closest;
	}
	
	// searchRecur(n, p)
	// if (n == null) // not found
	// 
	// if n.horizontal
	// if point is below node
		// search for closest below
		// if a point aboe could be closer than this, then search above
	// if point is above node
		// search for closest above
		// if a point below could be closer than this, the search left
	// if n.vertical
		// if point is left of node
			// search for closest on left
			// if a point on the right could be closer than this, then search right
		// if point is right of node
			// search for closest on right
			// if a point on the left could be closer than this, then search left

	// ok. at this point, we would have checked all possible nodes.
	// finally, return closest node
	
	 public boolean isEmpty() {
		 return (root == null);
	 }
public static void main(String[] args) {
	KdTree kdt = new KdTree();
	ArrayList<Point2D> points = new ArrayList<Point2D>();
	// test from assignment
	//	points.add(new Point2D(0.7,0.2));
	//	points.add(new Point2D(0.5,0.4));
	//	points.add(new Point2D(0.2,0.3));
	//	points.add(new Point2D(0.4,0.7));
	//	points.add(new Point2D(0.9,0.6));
	
	// Circle Test:
//  RectHV rect = new RectHV(.1, .05, .55, .8);
//	points.add(new Point2D( 0.206107 , 0.095492 ));
//	points.add(new Point2D( 0.206107 , 0.095492 ));
//	points.add(new Point2D( 0.975528 , 0.654508 ));
//	points.add(new Point2D( 0.024472 , 0.345492 ));
//	points.add(new Point2D( 0.793893 , 0.095492 ));
//	points.add(new Point2D( 0.793893 , 0.904508 ));
//	points.add(new Point2D( 0.975528 , 0.345492 ));
//	points.add(new Point2D( 0.206107 , 0.904508 ));
//	points.add(new Point2D( 0.500000 , 0.000000 ));
//	points.add(new Point2D( 0.024472 , 0.654508 ));
//	points.add(new Point2D( 0.500000 , 1.000000 ));
	
//	RectHV rect = new RectHV(0.0,.5,.5,.75);
//	points.add(new Point2D( 0.25 ,0.25 ));
//	points.add(new Point2D(  1.0, 0.75 ));
//	points.add(new Point2D(0.0  ,0.25  ));
//	points.add(new Point2D( 0.75 , 0.5 )); 
//	points.add(new Point2D(  0.25, 0.75 )); // E  0.25 0.75 this should be in rect according to autograder
//	StdOut.print("Does " + rect.toString() + " contain (0.25, 0.75)? " + rect.contains(new Point2D(0.25, 0.75)) + "\n");
//	for(Point2D p : points) {
//		kdt.insert(p);
//		//StdOut.print(kdt.size() + "\n");
//		//StdOut.print("Was " + p.toString() + " successfully inserted? " + kdt.contains(p) + '\n');
//		kdt.draw();
//		//StdOut.print("Drew " + p.toString() + '\n');
//	}
//
//	StdDraw.setPenColor(StdDraw.BLACK);
//	StdDraw.setPenRadius(0.01);
//	rect.draw();
//	StdOut.print("These points are in " + rect.toString() + "\n");
//	for (Point2D p : kdt.range(rect)) {
//		StdDraw.setPenColor(StdDraw.GREEN);
//		StdDraw.setPenRadius(0.03);
//		p.draw();
//		StdDraw.setPenColor(StdDraw.BLACK);
//		StdDraw.setPenRadius(0.015);
//		p.draw();
//		StdOut.print(p.toString() + "\n");
//	}
	
	
	
	/*
	 *   * input10.txt
    - student   nearest() = (0.499, 0.208)
    - reference nearest() = (0.499, 0.208)
    - performs incorrect traversal of kd-tree during call to nearest()
    - query point = (0.39, 0.19)
    - sequence of points inserted: 
      A  0.372 0.497
      B  0.564 0.413
      C  0.226 0.577
      D  0.144 0.179
      E  0.083 0.51
      F  0.32 0.708
      G  0.417 0.362
      H  0.862 0.825
      I  0.785 0.725
      J  0.499 0.208
    - student sequence of kd-tree nodes involved in calls to Point2D methods:
      A B G J C D E 
    - reference sequence of kd-tree nodes involved in calls to Point2D methods:
      A B G J C D 
    - failed on trial 4 of 1000
	 */
	points.add(new Point2D(0.7, 0.2));
	points.add(new Point2D(0.5, 0.4));
	points.add(new Point2D(0.2, 0.3));
	points.add(new Point2D(0.4, 0.7));
	points.add(new Point2D(0.9, 0.6));
	for(Point2D p : points) {
		kdt.insert(p);
		//StdOut.print(kdt.size() + "\n");
		//StdOut.print("Was " + p.toString() + " successfully inserted? " + kdt.contains(p) + '\n');
		kdt.draw();
		//StdOut.print("Drew " + p.toString() + '\n');
	}
	Point2D query = new Point2D(0.7, 0.936);
	StdDraw.setPenRadius(0.03);
	StdDraw.setPenColor(StdDraw.MAGENTA);
	query.draw();
	Point2D nearest = kdt.nearest(query);
	StdOut.print("The point nearest to " + query.toString() + " is " + nearest.toString() + "\n");
}

}
