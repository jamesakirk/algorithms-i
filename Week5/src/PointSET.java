/*
 * TODO Fix:
*/


import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.StdDraw;
import java.util.ArrayList;
import java.util.TreeSet;

public class PointSET {
	private final TreeSet<Point2D> points = new TreeSet<Point2D>();
	public PointSET() {}
	public boolean isEmpty() { return points.isEmpty(); }
	public int size() { return points.size(); }
	public void insert(Point2D p) { 
		if(p == null) throw new IllegalArgumentException();
		else points.add(p); 
	}
	public boolean contains(Point2D p) { 
		if (p == null) throw new IllegalArgumentException();
		if (points == null) return false;
		else return points.contains(p); 
	}
	public void draw() {
		StdDraw.setPenColor(StdDraw.BLACK);
		StdDraw.setPenRadius(0.01);
		for (Point2D p : points) {
			p.draw();
		}
	}
	public Point2D nearest(Point2D p) {
		if (p == null) throw new IllegalArgumentException();
		if (points == null) return null;
		if (points.isEmpty()) return null;
		Point2D nearest = points.first();
		for (Point2D q : points) {
			if (p.distanceTo(q) < p.distanceTo(nearest))
				nearest = q;
		}
		return nearest;
	}
	public Iterable<Point2D> range(RectHV rect) {
		if (rect == null) throw new IllegalArgumentException();
		if (points == null) throw new IllegalArgumentException();
		ArrayList<Point2D> inside = new ArrayList<Point2D>();
		for (Point2D p : points) {
			if (rect.contains(p))
				inside.add(p);
		}
		return inside;
	}
	public static void main(String[] args) {
		StdDraw.setPenColor(StdDraw.BLACK);
		StdDraw.setPenRadius(0.02);
		PointSET points = new PointSET();
		points.nearest(new Point2D(.1, .4));
		StdOut.print("Is this new PointSet empty? " + points.isEmpty() + '\n');
		StdOut.print("What is it's length? " + points.size() + '\n');
		points.insert(new Point2D(.5, .5));
		StdOut.print("Is this new PointSet empty? " + points.isEmpty() + '\n');
		StdOut.print("What is it's length? " + points.size() + '\n');
		StdOut.print("Does this contain (.3,.3)? " + points.contains(new Point2D(.3, .3)) + '\n');
		StdOut.print("Does this contain (.5,.5)? " + points.contains(new Point2D(.5, .5)) + '\n');
		points.insert(new Point2D(.1, .4));
		points.insert(new Point2D(.2, .5));
		points.insert(new Point2D(.3, .4));
		points.insert(new Point2D(.5, .4));
		points.draw();
		RectHV rect = new RectHV(.25, .3, .8, .8);
		rect.draw();
		StdDraw.setPenColor(StdDraw.BLUE);
		StdOut.print("The following points are in the box: \n");
		for (Point2D p : points.range(rect)) {
			StdOut.print(p.toString() + '\n');
			p.draw();
		}

		StdOut.print("What is the nearest point to (.1, .1)? " + points.nearest(new Point2D(.1, .1)).toString() + '\n');
		StdDraw.setPenColor(StdDraw.RED);
		points.nearest(new Point2D(.1, .1)).draw();

	}
}
