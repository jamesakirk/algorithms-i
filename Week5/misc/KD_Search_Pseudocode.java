	
	// insert p
	// if root == null then root = new Node (p);
	// else insertRecur(root, p)
	
	// insertRecur(node n, point p)
	// if n.vertical
		// if point is left of node
			// if n.left == null then n.left = new node(p)
			// else insertRecur(node n.left, point p)
		// if point is right of node
			// if n.right == null then n.right = new node(p)
			// insertRecur(node n.right, point p)
	// if n.horizontal
		// if point is below node
			// if n.left == null then n.left = new node(p)
			// else insertRecur(node n.left, point p)
		// if point is above node
			// if n.right == null then n.right = new node(p)
			// insertRecur(node n.right, point p)


	// search(p)
	// return searchRecur(root, p);
	
	// searchRecur(n, p)
	// if (n == null) // not found
	// if n.vertical
		// if point is left of node
			// search for closest on left
			// if a point on the right could be closer than this, then search right
		// if point is right of node
			// search for closest on right
			// if a point on the left could be closer than this, then search left
	// if n.horizontal
		// if point is below node
			// search for closest below
			// if a point aboe could be closer than this, then search above
		// if point is above node
			// search for closest above
			// if a point below could be closer than this, the search left
			
	// stack<Node> s = new stack<node>();
	// range (node n, rect r)
	// if (r.cotains(n.p)) s.add(n);
	// if (n.vertical)
		// if (r.xmin < node.x)
			// range (node.left)
		// if (node.x < r.max) 
			// range (node.right)
	// if (n.horizontal)
		// if (r.ymin < node.y)
			// range (node.left)
		// if (node.y < r.may) 
			// range (node.right)	
		
//nearest neighbor search
class KDTree {
	class Node{
		private Point2D p;
		private boolean vertical;
		private Node right;
		private Node left;
		private dist;
	}
	//public void add(Point2D p)
	//public Point2D findNearest(Point2D p)
	
private Node closerNode(Point2D p, Node currentNode, Node queryNode) {
	currentNode.dist = p.distanceTo(currentNode.p);
	queryNode.dist = p.distanceTo(queryNode.p);
		if(currentNode.dist < queryNode.dist){
			return currentNode;
		else
			return queryNode;
}	
	
	private Node searchForNearest(Point2D p, Node n) {
		if(p == null || n == null) throw new IllegalAccessException();
		Node closest = n;
		Node query;
		double curdist = p.distaneTo(n.p);

		if( n.vertical ) {
			if (p.x <= n.p.x) { 
				// query is to the left of our current point
				query = searchForNearest(Point2D p, Node n.left);
				closest = closerNode(p, n, query);
				dist = closestNode.dist;
				// We have searched the left subtree.
				// If this is closer than we can possibly get on the right subtree, we don't have to right.
				if (dist < (n.p.x - p.x) {
					// do nothing. nothing can be closer.
				} else {
					// othewise, there may be a closer point on the right subtree
					// search the right subtree:
					query = searchForNearest(Point2D p, Node n.right);
					closest = closerNode(p, n, query);
					dist = closestNode.dist;
				}
			}	
			else if (p.x > n.p.x) {
				// query is to the right of our current point
				query = searchForNearest(Point2D p, Node n.right);
				closest = closerNode(p, n, query);
				dist = closestNode.dist;
				// We have searched the left subtree.
				// If this is closer than we can possibly get on the right subtree, we don't have to right.
				if (dist < (p.x - n.p.x) {
					// do nothing. nothing can be closer.
				} else {
					// othewise, there may be a closer point on the right subtree
					// search the right subtree:
					query = searchForNearest(Point2D p, Node n.left);
					closest = closerNode(p, n, query);
					dist = closestNode.dist;
			}
		}
		else {
			// n is vertical node
			if (p.y <= n.p.y) { 
				// query is below our current point
				query = searchForNearest(Point2D p, Node n.left);
				closest = closerNode(p, n, query);
				dist = closestNode.dist;
				// We have searched the left subtree.
				// If this is closer than we can possibly get on the right subtree, we don't have to right.
				if (dist < (n.p.y - p.y) {
					// do nothing. nothing can be closer.
				} else {
					// othewise, there may be a closer point on the right subtree
					// search the right subtree:
					query = searchForNearest(Point2D p, Node n.right);
					closest = closerNode(p, n, query);
					dist = closestNode.dist;
				}
			}	
			else if (p.y > n.p.y) {
				// query is above our current point
				query = searchForNearest(Point2D p, Node n.right);
				closest = closerNode(p, n, query);
				dist = closestNode.dist;
				// We have searched the left subtree.
				// If this is closer than we can possibly get on the right subtree, we don't have to right.
				if (dist < (p.y - n.p.y) {
					// do nothing. nothing can be closer.
				} else {
					// othewise, there may be a closer point on the right subtree
					// search the right subtree:
					query = searchForNearest(Point2D p, Node n.left);
					closest = closerNode(p, n, query);
					dist = closestNode.dist;
			}
		}	
	}
	
		
		
		
		
		
		
		
		
		
		
		