import edu.princeton.cs.algs4.StdOut;
import java.lang.StringBuilder;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.ArrayList;

public class Board {
	
	private int[][] tiles;
	private int N, manhattanDistance, hammingDistance;

	// Public API
	public Board(int[][] tiles) {
		this.N = tiles.length;
		this.tiles = createClone(tiles);
		calculateHamming();
		calculateManhattan();	
	}
	
	// calculate the Hamming distance
	private void calculateHamming() {
		hammingDistance=0;
		for (int i = 0; i<this.N; i++) {
			for (int j=0; j<this.N; j++) {
				if (((i==j && (i==(N-1))) ? 0 : i * N + j + 1) != this.tiles[i][j] && this.tiles[i][j] != 0) {
					hammingDistance++;
				}
			}
		}
	}
	
	// calculate the Manhattan distance.
	private void calculateManhattan() {
		manhattanDistance = 0;
		for (int i = 0; i<N; i++) {
			for (int j=0; j<N; j++) {
				int val = tiles[i][j];
				if(val == 0) continue;
				Point home = findHome(val);
				int distancey = Math.abs(home.x - j);
				int distancex = Math.abs(home.y - i);	
				int distance_tot = distancex + distancey;
				manhattanDistance += distance_tot;
			}
		}
	}
	public String toString() { 
		String stringBoard;
		StringBuilder buildBoard= new StringBuilder();
		buildBoard.append(this.N);
		buildBoard.append('\n');
		for (int i = 0; i<this.N; i++) {
			for (int j=0; j<this.N; j++) {
				buildBoard.append(String.format("%2d ", tiles[i][j]));
			}
			buildBoard.append('\n');
		}
		stringBoard = buildBoard.toString();	
		return stringBoard; 
	}
	public int dimension() { return this.N; }
	public int hamming() { return hammingDistance; }
	public int manhattan() { return this.manhattanDistance; }
	public boolean isGoal() { return (0 == manhattan() ); }
	@Override
	public boolean equals(Object other) {
        if (other == this) return true;
        if (other == null) return false;
        if (other.getClass() != this.getClass()) return false;
		Board yBoard = (Board) other;
		return this.toString().equals(yBoard.toString());
	}
	public Iterable<Board> neighbors() {	
        ArrayList<Board> listOfNeighbors = new ArrayList<>();
    	Point zeroLocation = locateTile(0);
    	int xloc = zeroLocation.x;
    	int yloc = zeroLocation.y;
    	int[][] newTiles;
    	int temp;
    	//left: swap (x,y) and (x-1,y)
    	if(xloc-1 >= 0) {
        	newTiles = createClone(tiles);
        	temp = newTiles[xloc][yloc];
        	newTiles[xloc][yloc] = newTiles[xloc-1][yloc];
        	newTiles[xloc-1][yloc] = temp;
        	listOfNeighbors.add(new Board(newTiles)); 
    	}
    	//right: swap (x,y) and (x + 1,y)
    	if(xloc+1 < dimension()) {
        	newTiles = createClone(tiles);
        	temp = newTiles[xloc][yloc];
        	newTiles[xloc][yloc] = newTiles[xloc+1][yloc];
        	newTiles[xloc+1][yloc] = temp;
        	listOfNeighbors.add(new Board(newTiles)); 

    	}
    	//up: swap (x,y) and (x,y-1)6
    	if(yloc-1 >= 0) {
        	newTiles = createClone(tiles);
        	temp = newTiles[xloc][yloc];
        	newTiles[xloc][yloc] = newTiles[xloc][yloc-1];
        	newTiles[xloc][yloc-1] = temp;
        	listOfNeighbors.add(new Board(newTiles)); 

    	}
        	//down: swap (x,y) and (x,y+1)
        	if(yloc+1 < dimension()) {
	        	newTiles = createClone(tiles);
	        	temp = newTiles[xloc][yloc];
	        	newTiles[xloc][yloc] = newTiles[xloc][yloc+1];
	        	newTiles[xloc][yloc+1] = temp;
	        	listOfNeighbors.add(new Board(newTiles)); 
        	}
        	return listOfNeighbors;
        }
	public Board twin() {
		int[][] twinArray = createClone(this.tiles);
		int temp;
		if (twinArray[0][0] !=0 && twinArray[this.N-1][this.N-1] != 0) {
			// Can we swap on first diagonal?
			temp = twinArray[0][0];
			twinArray[0][0] = twinArray[this.N-1][this.N-1];
			twinArray[this.N-1][this.N-1] = temp;
			
		} else if (twinArray[0][this.N-1] !=0 && twinArray[this.N-1][0] != 0) {
			// Otherwise, can we swap on other diagonal?
			temp = twinArray[0][this.N-1];
			twinArray[0][this.N-1] = twinArray[this.N-1][0];
			twinArray[this.N-1][0] = temp;
		} else {
			throw new IllegalArgumentException();
		}
		Board twinIs = new Board(twinArray);
		return twinIs;
	}

	// Helper classes and methods
	private class Point {	
		private int x,y;
		Point(int x, int y){
			this.x = x;
			this.y = y;
		}
	}
	private Point findHome(int lostTile) {
		if (lostTile == 0) {
			return new Point (this.N - 1,this.N - 1);
		}
		if (lostTile >= this.N*this.N) throw new IndexOutOfBoundsException();
		if (lostTile < 0) throw new IndexOutOfBoundsException();
		int x = (lostTile - 1) % this.N;
		int y = (lostTile - x) / this.N;
		return new Point (x,y);
	}
	private Point locateTile(int queryTile) {
		for (int i = 0; i<this.N; i++) {
			for (int j=0; j<this.N; j++) {
					if (queryTile == this.tiles[i][j]) return new Point (i,j);
				}
			}
		throw new IllegalArgumentException();
	}
    private void swapVals (int q, int p) {
    	Point ploc = this.locateTile(p);
    	Point qloc = this.locateTile(q);
    	int temp = this.tiles[ploc.x][ploc.y];
    	this.tiles[ploc.x][ploc.y] = this.tiles[qloc.x][qloc.y];
    	this.tiles[qloc.x][qloc.y] = temp;
		calculateManhattan();
		calculateHamming();
    }
    private int[][] createClone(int[][] original){
    	int[][] clonedArray = new int[original.length][original[0].length];
    	for (int i = 0; i<original.length; i++) {
    		for (int j = 0; j < original[0].length; j++) {
    			clonedArray[i][j] = original[i][j];
    		}
    	}
    	return clonedArray;
    }
	public static void main(String[] args) {
		int N = 3;
		int[][] tiles = new int[N][N];
		for (int i = 0; i<N; i++) {
			for (int j=0; j<N; j++) {
				tiles[i][j] = N*i + j + 1;
			}
		}
		tiles[N-1][N-1] = 0;
		Board testBoard = new Board(tiles).twin();
		StdOut.print("This board is " + testBoard.dimension() + "x" + N + '\n');
		StdOut.print(testBoard.toString());
		StdOut.print("Hamming: " + testBoard.hamming() + '\n');
		StdOut.print("Manhattan: " + testBoard.manhattan() + '\n');
		StdOut.print("Is this the goal? : " + testBoard.isGoal() + '\n');
		Board twin = testBoard.twin();
		StdOut.print("The twin is " + twin.dimension() + "x" + N + '\n');
		StdOut.print(twin.toString());		
		StdOut.print("Is the original the same as the twin? " + twin.equals(testBoard) + '\n');	
		int[][] equalTile = new int[N][N];
		for (int i = 0; i<N; i++) {
			for (int j=0; j<N; j++) {
				equalTile[i][j] = N*i + j + 1;
			}
		}
		equalTile[N-1][N-1] = 0;
		Board equalBoard = new Board(equalTile);
		int tilea=8, tileb=0;
		equalBoard.swapVals(tilea, tileb);
		StdOut.print("OK, new board:  " + equalBoard.dimension() + "x" + N + '\n');
		StdOut.print(equalBoard.toString());
		// OK, test out solving it.
		StdOut.print("Is this board the same as the original? " + equalBoard.equals(testBoard) + '\n');
		StdOut.print("Can we calculate the neighbors? \n");
		for(Board currentBoard : testBoard.neighbors()){
			StdOut.print("Here is a neighbor: \n");
			StdOut.print(currentBoard.toString());
		}
	}
}
	
