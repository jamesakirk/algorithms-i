import java.util.Comparator;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.Stack;
import edu.princeton.cs.algs4.MinPQ;

public class Solver {
	private Stack<Board> solution = new Stack<Board>();
	private MinPQ<SearchNode> primaryPQ, twinPQ;
	private Boolean solvable;
	private int solverMoves;	
	public Solver(Board initial) {
		if (initial == null) throw new IllegalArgumentException();
		SearchNode root = new SearchNode(initial, null);
		primaryPQ = new MinPQ<SearchNode>(root.priorityOrder());
		primaryPQ.insert(root);
		SearchNode twin = new SearchNode(initial.twin(), null);
		twinPQ = new MinPQ<SearchNode>(twin.priorityOrder());
		twinPQ.insert(twin);
		SearchNode currentPrimaryNode = root;
		SearchNode currentTwinNode = twin;
		while(!primaryPQ.min().isGoal && !twinPQ.min().isGoal) {	
			currentPrimaryNode = primaryPQ.delMin();
			currentTwinNode = 	twinPQ.delMin();			
			for(Board currentNeighbor : currentPrimaryNode.currentBoard.neighbors()) {
				if(currentPrimaryNode.previousSearchNode != null && 
						currentNeighbor.equals(currentPrimaryNode.previousSearchNode.currentBoard)) continue;
				SearchNode neighborNode = new SearchNode(currentNeighbor, currentPrimaryNode);
				if ((currentPrimaryNode.previousSearchNode == null) || !neighborNode.currentBoard.equals(currentPrimaryNode.previousSearchNode.currentBoard)) {
					primaryPQ.insert(neighborNode);
				}
			}	
			for(Board currentNeighbor : currentTwinNode.currentBoard.neighbors()) {
				if(currentTwinNode.previousSearchNode != null &&
						currentNeighbor.equals(currentTwinNode.previousSearchNode.currentBoard)) continue;
				SearchNode neighborNode = new SearchNode(currentNeighbor, currentTwinNode);
				if ((currentTwinNode.previousSearchNode == null) || !neighborNode.currentBoard.equals(currentTwinNode.previousSearchNode.currentBoard)) {
					twinPQ.insert(neighborNode);
				}
			}
			solvable = solvable;
		}
		currentPrimaryNode = primaryPQ.delMin();
		currentTwinNode = 	twinPQ.delMin();	
		if (currentPrimaryNode.currentBoard.isGoal() == true ) {
			// is solvable
			solvable = true;
			solverMoves = currentPrimaryNode.distanceFromBeginning;
			while (currentPrimaryNode != null) {
				solution.push(currentPrimaryNode.currentBoard);
				currentPrimaryNode = currentPrimaryNode.previousSearchNode;
			};
		} else {
			solvable = false;
		}

	}

	public boolean isSolvable() {

		return solvable;
	}

	// min number of moves to solve initial board; -1 if unsolvable
	public int moves() {
		return isSolvable() ? solverMoves : -1;
	}

	// return moves.
	public Iterable<Board> solution() {
		return isSolvable() ? solution : null;
	}

	private class SearchNode {
		private Board currentBoard;
		private int distanceFromBeginning;
		private int priority, hamming, manhattan;
		private SearchNode previousSearchNode;
		private boolean isGoal;

		public SearchNode(Board currentBoard, SearchNode prevNode) {
			this.currentBoard = currentBoard;
			this.previousSearchNode = prevNode;
			this.isGoal = this.currentBoard.isGoal();
			this.hamming = currentBoard.hamming();
			this.manhattan = currentBoard.manhattan();
			this.distanceFromBeginning = (previousSearchNode == null) ? 0
					: previousSearchNode.distanceFromBeginning + 1;
			this.priority = this.manhattan + this.distanceFromBeginning;
		}
		public int compareTo(SearchNode that) {
			int returnValue = this.priority -  that.priority;
	    	return returnValue;
		}

		public Comparator<SearchNode> priorityOrder() {
			/* YOUR CODE HERE */
			return new SearchNodeComparator();
		}

		private class SearchNodeComparator implements Comparator<SearchNode> {
			// private final Point point;
			//    	SlopeComparator(Point point){
			//    		this.point = point;
			//    	}
			@Override
			public int compare(SearchNode a, SearchNode b) {
				if (a.priority > b.priority)
					return 1;
				else if (a.priority < b.priority)
					return -1;
				else
					return 0;
			}
		}
	}

	// ### Main Procedure.... each step here will be duplicated###
	// this.root = root: define root

	// process(both root) where process is:
	// Process(node currentNode)
	// if Goal():
	// it is solved, so:
	// mark as solvable
	// mark as goal
	// walk back pointers until current == root;
	// keep a counter on this distance, or used the distance on the searchNode. It
	// shouldn't matter which. that will be returned via modes()
	// else, keep searching
	// create neighbor boards
	// create neighbor searchNodes
	// if neighbor board is same as prior board, do not create neighbor
	// add searchNodes 				StdOut.println('\n'+currentNode.currentBoard.toString());to PQ
	// Process (lowest-priority node in the PQ)


	
//	public static void main(String[] args) {
//		int N = 3;
//		int[][] tiles = new int[N][N];
//		for (int i = 0; i < N; i++) {
//			for (int j = 0; j < N; j++) {
//				tiles[i][j] = N * i + j + 1;
//			}
//		}
//		tiles[N - 1][N - 1] = 0;
//		
//		Board testBoard = new Board(tiles);
//		testBoard.swapVals(0,6);
//		testBoard.swapVals(0,3);
//		testBoard.swapVals(0,2);
//		// StdOut.print("Initial Manhattan: " + initial.manhattan());
//		Solver solver = new Solver(testBoard);
//		// StdOut.println(filename + ": " + solver.moves());
//		// }
//	}
	public static void main(String[] args) {

	    // create initial board from file
	    In in = new In(args[0]);
	    int n = in.readInt();
	    int[][] tiles = new int[n][n];
	    for (int i = 0; i < n; i++)
	        for (int j = 0; j < n; j++)
	            tiles[i][j] = in.readInt();
	    Board initial = new Board(tiles);

	    // solve the puzzle
	    Solver solver = new Solver(initial);

	    // print solution to standard output
	    if (!solver.isSolvable())
	        StdOut.println("No solution possible");
	    else {
	        StdOut.println("Minimum number of moves = " + solver.moves() + "\n");
	        for (Board board : solver.solution())
	            StdOut.println(board);
	    }
	}
}
