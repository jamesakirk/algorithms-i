import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.In;
import java.util.Arrays;

public class FastCollinearPoints {
	private Point[] pointArray;
	private LineSegment[] segmentArray = new LineSegment[0];
	
	public FastCollinearPoints(Point[] points) {
		if (points == null) throw new IllegalArgumentException();
		for (int i = 0; i<points.length; i++) {
			if (points[i] == null) throw new IllegalArgumentException();
			else for (int j = 0; j < i; j++ ) {
				if (points[i].compareTo(points[j]) == 0) throw new IllegalArgumentException();
			}
		}
		pointArray = points.clone();
		for (int i = 0; i < pointArray.length; i++) {
			Point currentPoint = points[i];      
	        Arrays.sort(pointArray, currentPoint.slopeOrder());	   
	        int j = 1;
	        while (j < pointArray.length) {
		        int slopeCount = 0;
		        double currentSlope = points[i].slopeTo(pointArray[j]);
		        while(j < pointArray.length && currentSlope == points[i].slopeTo(pointArray[j])) {
		        	slopeCount++;
		        	j++;
		        }
		        if(slopeCount >= 3) {
		        	//Cut and DO NOT include this one.			
					Point[] pointsInLine = new Point[slopeCount + 1];
					for (int copyFrom = j - (slopeCount), copyTo = 0; copyTo < slopeCount; copyTo++, copyFrom++) {
						pointsInLine[copyTo] = pointArray[copyFrom];
					}
					pointsInLine[slopeCount] = points[i];
					Point minPoint = pointsInLine[0];
					Point maxPoint = pointsInLine[0];						
					for (int k=0;k < pointsInLine.length; k++) {
						Point p = pointsInLine[k];
						maxPoint = (p.compareTo(maxPoint) > 0) ? p : maxPoint;
						minPoint = (p.compareTo(minPoint) < 0) ? p : minPoint;
					}
					if(points[i].compareTo(minPoint) == 0) {
						LineSegment newCollection[] = new LineSegment[segmentArray.length + 1];
						for (int n = 0; n<segmentArray.length; n++) newCollection[n] = segmentArray[n];
						newCollection[segmentArray.length] = new LineSegment (minPoint, maxPoint);
						segmentArray = newCollection;
					}
		        }  
	        }
		}
	}
	
	public int numberOfSegments() {
		return segmentArray.length; // the number of line segments
	}
	public LineSegment[] segments() {
		return segmentArray.clone(); // the line segments
	}
	public static void main(String[] args) {
	    // read the n points from a file
	    In in = new In(args[0]);
	    int n = in.readInt();
	    Point[] points = new Point[n];
	    for (int i = 0; i < n; i++) {
	        int x = in.readInt();
	        int y = in.readInt();
	        points[i] = new Point(x, y);
	    }

	    // draw the points
	    StdDraw.enableDoubleBuffering();
	    StdDraw.setXscale(0, 32768);
	    StdDraw.setYscale(0, 32768);        		        	
	    
	    StdDraw.setPenRadius(.02);
	    for (Point p : points) {
	        p.draw();
	    }
	    StdDraw.show();

	    StdDraw.setPenRadius(.005);
	    // print and draw the line segments
	    FastCollinearPoints FCP = new FastCollinearPoints(points);
	    for (LineSegment segment : FCP.segments()) {
	        StdOut.println(segment);
	        segment.draw();
	    }
	    StdDraw.show();
		}

}


