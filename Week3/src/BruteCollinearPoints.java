import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.In;
import java.util.Arrays;

public class BruteCollinearPoints {
	private Point[] pointArray;
	private LineSegment[] segmentArray = new LineSegment[0];

	public BruteCollinearPoints(Point[] points) {
		if (points == null)
			throw new IllegalArgumentException();
		for (int i = 0; i < points.length; i++) {
			if (points[i] == null)
				throw new IllegalArgumentException();
			else
				for (int j = 0; j < i; j++) {
					if (points[i].compareTo(points[j]) == 0)
						throw new IllegalArgumentException();
				}
		}
		pointArray = points.clone();
		Arrays.sort(pointArray);
		LineSegment newSegmentArray[] = new LineSegment[0];
		for (int i = 0; i < pointArray.length; i++) {
			for (int j = i + 1; j < pointArray.length; j++) {
				for (int k = j + 1; k < pointArray.length; k++) {
					double slope_ij = pointArray[i].slopeTo(pointArray[j]);
					double slope_ik = pointArray[i].slopeTo(pointArray[k]);
					if (slope_ij != slope_ik)
						continue;
					for (int l = pointArray.length - 1; l > k; l--) {
						double slope_il = pointArray[i].slopeTo(pointArray[l]);
						if (slope_ij == slope_ik && slope_ij == slope_il) {
							newSegmentArray = new LineSegment[segmentArray.length + 1];
							for (int n = 0; n < segmentArray.length; n++)
								newSegmentArray[n] = segmentArray[n];
							newSegmentArray[segmentArray.length] = new LineSegment(pointArray[i], pointArray[l]);
							segmentArray = newSegmentArray;

						}

					}
				}
			}
		}
	}

	public int numberOfSegments() {
		return segmentArray.length; // the number of line segments
	}

	public LineSegment[] segments() {
		return segmentArray.clone(); // the line segments
	}

	public static void main(String[] args) {
		// read the n points from a file
		In in = new In(args[0]);
		int n = in.readInt();
		Point[] points = new Point[n];
		for (int i = 0; i < n; i++) {
			int x = in.readInt();
			int y = in.readInt();
			points[i] = new Point(x, y);
		}

		// draw the points
		StdDraw.enableDoubleBuffering();
		StdDraw.setXscale(0, 32768);
		StdDraw.setYscale(0, 32768);
		StdDraw.setPenRadius(.02);
		for (Point p : points) {
			p.draw();
		}
		StdDraw.show();

		StdDraw.setPenRadius(.005);
		// print and draw the line segments
		BruteCollinearPoints BCP = new BruteCollinearPoints(points);
		for (LineSegment segment : BCP.segments()) {
			StdOut.println(segment);
			segment.draw();
		}
		StdDraw.show();
	}

}
