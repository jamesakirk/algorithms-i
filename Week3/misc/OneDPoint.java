package TestingBranch;
/******************************************************************************
 *  Compilation:  javac OneDPoint.java
 *  Execution:    java OneDPoint
 *  Dependencies: none
 *  
 *  An immutable data type for points in the plane.
 *  For use on Coursera, Algorithms Part I programming assignment.
 *
 ******************************************************************************/

import java.util.Comparator;
import java.util.Arrays;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;

public class OneDPoint implements Comparable<OneDPoint> {

    private final int x;     // x-coordinate of this point
    public OneDPoint(int x) {
        /* DO NOT MODIFY */
        this.x = x;
    }
    public void draw() {
        /* DO NOT MODIFY */
        StdDraw.point(x, 0);
    }
    public void drawTo(OneDPoint that) {
        /* DO NOT MODIFY */
        StdDraw.line(this.x, 0, that.x, 0);
    }
    public double distance(OneDPoint that) {
		return this.x-that.x;
    }
    public int compareTo(OneDPoint that) {
    	int returnValue = this.x - that.x;
    	return returnValue;
    }
    public Comparator<OneDPoint> slopeOrder() {
        /* YOUR CODE HERE */
    	return new DistComparator();
    }
    private class DistComparator implements Comparator<OneDPoint> {   	
    	public int compare(OneDPoint a, OneDPoint b) {
    		double distA = distance(a);
    		double distB = distance(b);
    		if (distA  == distB) return 0;
    		else if (distA > distB) return 1;
    		else return -1;
    	}
    }

    public String toString() {
        return "(" + x + ")";
    }
    public static void main(String[] args) {
    	    In in = new In(args[0]);
    	    int n = in.readInt();
    	    OneDPoint[] points = new OneDPoint[n];
    	    for (int i = 0; i < n; i++) {
    	        int x = in.readInt();
    	        points[i] = new OneDPoint(x);
    	    }
    	    OneDPoint[] sortedPoints = points.clone();
    	    for (int i = 0; i < n; i++) {
               	Arrays.sort(sortedPoints, points[i].slopeOrder());
               	StdOut.print("Current Point: " + points[i].toString() + '\n');
               	for(int j=0; j<sortedPoints.length; j++) {
                   		StdOut.print("Compared to point: " + sortedPoints[j].toString() + " Distance is : " + points[i].distance(sortedPoints[j]) + '\n');
               	}

    	    }


    	    // draw the points
    	    StdDraw.enableDoubleBuffering();
    	    StdDraw.setXscale(0, 32768);
    	    StdDraw.setYscale(0, 32768);
    	    StdDraw.setPenRadius(.02);
    	    for (OneDPoint p : points) {
    	        p.draw();
    	    }
    	    StdDraw.show();
    }
}
